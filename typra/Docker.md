uname -r  

查看内核版本

cat /etc/os-release

系统信息



# 一、Docker安装

1、卸载旧的版本

```shell
yum remove docker \
                  docker-client \
                  docker-client-latest \
                  docker-common \
                  docker-latest \
                  docker-latest-logrotate \
                  docker-logrotate \
                  docker-engine
```

2、需要的安装包

```shell
yum install -y yum-utils
```

3、设值镜像仓库

```shell
 yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo    #默认是国外的
 
 yum-config-manager \
    --add-repo \
    http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo    #推荐使用阿里云的
    
  #更新yum包索引
  yum makecache fast
```

4、安装docker相关

```shell
# ce 社区版 ee 企业版
yum install docker-ce docker-ce-cli containerd.io
```

5、启动docker

```shell
systemctl start docker

#查看版本
docker version

# 运行hello world
docker run hello-world

```

6、查看docker镜像

```shell
dokcer images
```

7、卸载docker

```shell
# 删除软件
yum remove docker-ce docker-ce-cli containerd.io
# 删除目录
rm -rf /var/lib/docker
rm -rf /var/lib/containerd
```

8 、配置阿里云镜像加速

```shell
# 创建目录
sudo mkdir -p /etc/docker

# 设置仓库
sudo tee /etc/docker/daemon.json <<-'EOF'
{
  "registry-mirrors": ["https://k264tp66.mirror.aliyuncs.com"]
}
EOF

# 重新加载
sudo systemctl daemon-reload
# 重启
sudo systemctl restart docker
```

# 二、Docker 常用命令

```shell
docker version    # docker版本信息
docker info       # docker的系统信息，包括镜像和容器的数量
docker help       # 帮助命令
```

## 2.1 镜像命令

**docker images 查看所有的本地主机上的镜像**

```shell
[root@iZuf67gjh27h6gyq7gvprtZ ~]# docker images
REPOSITORY        TAG       IMAGE ID       CREATED       SIZE
hello-world       latest    feb5d9fea6a5   4 weeks ago   13.3kB
jenkins/jenkins   latest    482543872bd9   6 weeks ago   441MB

# 解释
REPOSITORY  镜像的仓库源
TAG			镜像的标签
IMAGE ID	镜像的Id
CREATED		镜像的创建时间
SIZE		镜像的大小

# 可选项
Options:
  -a, --all             # 列出所有的镜像
  -q, --quiet           # 只显示镜像Id

```

**docker search 搜索镜像**

```shell
[root@iZuf67gjh27h6gyq7gvprtZ ~]# docker search mysql
NAME                              DESCRIPTION                                     STARS     OFFICIAL   AUTOMATED
mysql                             MySQL is a widely used, open-source relation…   11587     [OK]       
mariadb                           MariaDB Server is a high performing open sou…   4407      [OK]       

#加STARS过滤条件
[root@iZuf67gjh27h6gyq7gvprtZ ~]# docker search mysql --filter=stars=5000
NAME      DESCRIPTION                                     STARS     OFFICIAL   AUTOMATED
mysql     MySQL is a widely used, open-source relation…   11587     [OK]       


```

**docker pull 拉取镜像**

```shell
# 下载镜像 docker pull 镜像名[:tag]
[root@iZuf67gjh27h6gyq7gvprtZ ~]# docker pull mysql
Using default tag: latest		# 如果不指定tag，默认是latest
latest: Pulling from library/mysql
b380bbd43752: Pull complete 		# 分层下载，docker image的核心 联合文件系统
f23cbf2ecc5d: Pull complete 
30cfc6c29c0a: Pull complete 
b38609286cbe: Pull complete 
8211d9e66cd6: Pull complete 
2313f9eeca4a: Pull complete 
7eb487d00da0: Pull complete 
4d7421c8152e: Pull complete 
77f3d8811a28: Pull complete 
cce755338cba: Pull complete 
69b753046b9f: Pull complete 
b2e64b0ab53c: Pull complete 
Digest: sha256:6d7d4524463fe6e2b893ffc2b89543c81dec7ef82fb2020a1b27606666464d87	#签名
Status: Downloaded newer image for mysql:latest
docker.io/library/mysql:latest	# 真是地址

# 等价于
docker pull mysql
docker pull docker.io/library/mysql:latest

[root@iZuf67gjh27h6gyq7gvprtZ ~]# docker pull mysql:5.7
5.7: Pulling from library/mysql
b380bbd43752: Already exists # 文件可以共用
f23cbf2ecc5d: Already exists 
30cfc6c29c0a: Already exists 
b38609286cbe: Already exists 
8211d9e66cd6: Already exists 
2313f9eeca4a: Already exists 
7eb487d00da0: Already exists 
a71aacf913e7: Pull complete 
393153c555df: Pull complete 
06628e2290d7: Pull complete 
ff2ab8dac9ac: Pull complete 
Digest: sha256:2db8bfd2656b51ded5d938abcded8d32ec6181a9eae8dfc7ddf87a656ef97e97
Status: Downloaded newer image for mysql:5.7
docker.io/library/mysql:5.7


```

 **docker rmi 删除镜像**

```shell
[root@iZuf67gjh27h6gyq7gvprtZ ~]# docker rmi 镜像Id						   # 删除指定的镜像
[root@iZuf67gjh27h6gyq7gvprtZ ~]# docker rmi 镜像Id 镜像Id 镜像Id				# 删除多个镜像 
[root@iZuf67gjh27h6gyq7gvprtZ ~]# docker rmi -f $(docker images -aq)		# 删除全部镜像
```

## 2.2 容器命令

说明：我们有了镜像才能创建容器，linux下载一个centos镜像学习

```shell
docker pull centos
```

新建容器并启动

```shell
docker run [可选参数] image

# 参数说明
--name="Name"	容器的名称 tomcat01 tomcat02 用来区别容器
-d				后台方式运行
-it				使用交互方式运行，进入容器查看内容
-p				指定容器的端口 -p 8080:8080
	-p ip:主机端口:容器端口
	-p 主机端口:容器端口
	-p 容器端口
	容器端口
-P				随机指定端口

[root@iZuf67gjh27h6gyq7gvprtZ ~]# docker images
REPOSITORY   TAG       IMAGE ID       CREATED       SIZE
centos       latest    5d0da3dc9764   5 weeks ago   231MB
# 测试启动并进入容器
[root@iZuf67gjh27h6gyq7gvprtZ ~]# docker run -it centos /bin/bash
[root@cbd19c2a8862 /]# ls
bin  dev  etc  home  lib  lib64  lost+found  media  mnt  opt  proc  root  run  sbin  srv  sys  tmp  usr  var
# 从容器中退回主机
[root@cbd19c2a8862 /]# exit
exit
```

**列出所有运行的容器**

```shell
# docker ps命令
		# 列出当前正在运行的容器
-a  	# 列出当前正在运行的容器+历史运行过的容器
-n=? 	# 显示最近创建过的容器 n是个数
-q		# 只显示容器的编号
[root@iZuf67gjh27h6gyq7gvprtZ ~]# docker ps -a
CONTAINER ID   IMAGE          COMMAND                  CREATED         STATUS                     PORTS     NAMES
cbd19c2a8862   centos         "/bin/bash"              8 minutes ago   Exited (0) 5 minutes ago             friendly_payne
6c8204373925   feb5d9fea6a5   "/hello"                 7 days ago      Exited (0) 7 days ago                naughty_saha
43e023a7c427   482543872bd9   "/sbin/tini -- /usr/…"   6 weeks ago     Exited (143) 7 days ago              jenkins
[root@iZuf67gjh27h6gyq7gvprtZ ~]# docker ps -n=2
CONTAINER ID   IMAGE          COMMAND       CREATED         STATUS                     PORTS     NAMES
cbd19c2a8862   centos         "/bin/bash"   8 minutes ago   Exited (0) 6 minutes ago             friendly_payne
6c8204373925   feb5d9fea6a5   "/hello"      7 days ago      Exited (0) 7 days ago                naughty_saha
[root@iZuf67gjh27h6gyq7gvprtZ ~]# docker ps -aq
cbd19c2a8862
6c8204373925
43e023a7c427
[root@iZuf67gjh27h6gyq7gvprtZ ~]# 

```
**退出容器**

```shell
exit		#直接容器停止并退出
ctrl + p + q # 容器不停止退出

[root@iZuf67gjh27h6gyq7gvprtZ ~]# docker ps
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES
[root@iZuf67gjh27h6gyq7gvprtZ ~]# docker run -it centos /bin/bash
[root@c5e062e281bd /]# [root@iZuf67gjh27h6gyq7gvprtZ ~]#   # ctrl + p + q退出
[root@iZuf67gjh27h6gyq7gvprtZ ~]# docker ps
CONTAINER ID   IMAGE     COMMAND       CREATED          STATUS          PORTS     NAMES
c5e062e281bd   centos    "/bin/bash"   17 seconds ago   Up 17 seconds             zen_buck
[root@iZuf67gjh27h6gyq7gvprtZ ~]# 

```

**删除容器**

```shell
docker rm 容器id					# 删除指定的容器，不能删除正在运行的容器，只能用docker rm -f删除
docker rm -f $(docker ps -aq)    # 删除所有的容器 
docker ps -a -q|xargs docker rm  # 删除所有镜像
```

**启动和停止容器**

```shell
docker start 容器id       # 启动容器
docker restart 容器id     # 重启容器
docker stop 容器id        # 停止正在运行容器
docker kill 容器id        # 强制停止正在运行的容器 

[root@iZuf67gjh27h6gyq7gvprtZ ~]# docker run -it centos /bin/bash
[root@84e8e1d42d9b /]# exit
exit
[root@iZuf67gjh27h6gyq7gvprtZ ~]# docker ps
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES
[root@iZuf67gjh27h6gyq7gvprtZ ~]# docker ps -a
CONTAINER ID   IMAGE     COMMAND       CREATED          STATUS                     PORTS     NAMES
84e8e1d42d9b   centos    "/bin/bash"   13 seconds ago   Exited (0) 9 seconds ago             quirky_hofstadter
[root@iZuf67gjh27h6gyq7gvprtZ ~]# docker start 84e8e1d42d9b
84e8e1d42d9b
[root@iZuf67gjh27h6gyq7gvprtZ ~]# docker ps
CONTAINER ID   IMAGE     COMMAND       CREATED          STATUS         PORTS     NAMES
84e8e1d42d9b   centos    "/bin/bash"   48 seconds ago   Up 2 seconds             quirky_hofstadter
[root@iZuf67gjh27h6gyq7gvprtZ ~]# docker stop 84e8e1d42d9b
84e8e1d42d9b
[root@iZuf67gjh27h6gyq7gvprtZ ~]# docker ps
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES
[root@iZuf67gjh27h6gyq7gvprtZ ~]# 

```

## 2.3 常用的其他命令

**后台启动容器**

```shell
# 命令 docker run -d 镜像名
[root@iZuf67gjh27h6gyq7gvprtZ ~]# docker run -d centos
9c6be9635a94b41b1bec722e66780de3e5ffbce03b3800e52b8d6ab174782303

# 问题 docker ps,发现 centos停止了

# 常见的坑：docker容器使用后台启动，就必须要有一个前台进程。docker发现没有应用就会自动停止
# nginx,容器启动后，发现自己没有提供服务，就会立即停止，就是没有服务了

```

**查看日志**

```shell
docker logs -f -t --tail 容器id

# 自己编写一个shell脚本，制造日志
docker run -d centos /bin/sh -c "while true;do echo kuangshen; sleep 1; done "
a4ad726a09d48cc3d65fef7a5083f076ee84871f2826235db2f3a5d13423e44d
[root@iZuf67gjh27h6gyq7gvprtZ ~]# docker ps
CONTAINER ID   IMAGE     COMMAND                  CREATED         STATUS         PORTS     NAMES
a4ad726a09d4   centos    "/bin/sh -c 'while t…"   6 seconds ago   Up 5 seconds             serene_maxwell
[root@iZuf67gjh27h6gyq7gvprtZ ~]# docker logs -f -t --tail 10 a4ad726a09d4
2021-10-30T11:10:47.945303567Z kuangshen
2021-10-30T11:10:48.947338530Z kuangshen
2021-10-30T11:10:49.949328395Z kuangshen
2021-10-30T11:10:50.951301414Z kuangshen
2021-10-30T11:10:51.953228332Z kuangshen
2021-10-30T11:10:52.955136187Z kuangshen
2021-10-30T11:10:53.957003352Z kuangshen
2021-10-30T11:10:54.959337772Z kuangshen
2021-10-30T11:10:55.961394510Z kuangshen
^C
[root@iZuf67gjh27h6gyq7gvprtZ ~]# docker ps
CONTAINER ID   IMAGE     COMMAND                  CREATED              STATUS              PORTS     NAMES
a4ad726a09d4   centos    "/bin/sh -c 'while t…"   About a minute ago   Up About a minute             serene_maxwell
    
```

**查看容器中的进程信息**

``` shell
# docker top 容器id
[root@iZuf67gjh27h6gyq7gvprtZ ~]# docker top a4ad726a09d4
UID                 PID                 PPID                C                   STIME               TTY     
root                24611               24591               0                   19:10               ?      
root                24840               24611               0                   19:11               ?   
```

**查看镜像的元数据**

```shell
# 命令
docker inspect 容器id
#测试
[root@iZuf67gjh27h6gyq7gvprtZ ~]# docker inspect a4ad726a09d4
[
    {
        "Id": "a4ad726a09d48cc3d65fef7a5083f076ee84871f2826235db2f3a5d13423e44d",
        "Created": "2021-10-30T11:10:01.595135073Z",
        "Path": "/bin/sh",
        "Args": [
            "-c",
            "while true;do echo kuangshen; sleep 1; done "
        ],
        "State": {
            "Status": "running",
            "Running": true,
            "Paused": false,
            "Restarting": false,
            "OOMKilled": false,
            "Dead": false,
            "Pid": 24611,
            "ExitCode": 0,
            "Error": "",
            "StartedAt": "2021-10-30T11:10:01.85731623Z",
            "FinishedAt": "0001-01-01T00:00:00Z"
        },
        "Image": "sha256:5d0da3dc976460b72c77d94c8a1ad043720b0416bfc16c52c45d4847e53fadb6",
        "ResolvConfPath": "/var/lib/docker/containers/a4ad726a09d48cc3d65fef7a5083f076ee84871f2826235db2f3a5d13423e44d/resolv.conf",
        "HostnamePath": "/var/lib/docker/containers/a4ad726a09d48cc3d65fef7a5083f076ee84871f2826235db2f3a5d13423e44d/hostname",
        "HostsPath": "/var/lib/docker/containers/a4ad726a09d48cc3d65fef7a5083f076ee84871f2826235db2f3a5d13423e44d/hosts",
        "LogPath": "/var/lib/docker/containers/a4ad726a09d48cc3d65fef7a5083f076ee84871f2826235db2f3a5d13423e44d/a4ad726a09d48cc3d65fef7a5083f076ee84871f2826235db2f3a5d13423e44d-json.log",
        "Name": "/serene_maxwell",
        "RestartCount": 0,
        "Driver": "overlay2",
        "Platform": "linux",
        "MountLabel": "",
        "ProcessLabel": "",
        "AppArmorProfile": "",
        "ExecIDs": null,
        "HostConfig": {
            "Binds": null,
            "ContainerIDFile": "",
            "LogConfig": {
                "Type": "json-file",
                "Config": {}
            },
            "NetworkMode": "default",
            "PortBindings": {},
            "RestartPolicy": {
                "Name": "no",
                "MaximumRetryCount": 0
            },
            "AutoRemove": false,
            "VolumeDriver": "",
            "VolumesFrom": null,
            "CapAdd": null,
            "CapDrop": null,
            "CgroupnsMode": "host",
            "Dns": [],
            "DnsOptions": [],
            "DnsSearch": [],
            "ExtraHosts": null,
            "GroupAdd": null,
            "IpcMode": "private",
            "Cgroup": "",
            "Links": null,
            "OomScoreAdj": 0,
            "PidMode": "",
            "Privileged": false,
            "PublishAllPorts": false,
            "ReadonlyRootfs": false,
            "SecurityOpt": null,
            "UTSMode": "",
            "UsernsMode": "",
            "ShmSize": 67108864,
            "Runtime": "runc",
            "ConsoleSize": [
                0,
                0
            ],
            "Isolation": "",
            "CpuShares": 0,
            "Memory": 0,
            "NanoCpus": 0,
            "CgroupParent": "",
            "BlkioWeight": 0,
            "BlkioWeightDevice": [],
            "BlkioDeviceReadBps": null,
            "BlkioDeviceWriteBps": null,
            "BlkioDeviceReadIOps": null,
            "BlkioDeviceWriteIOps": null,
            "CpuPeriod": 0,
            "CpuQuota": 0,
            "CpuRealtimePeriod": 0,
            "CpuRealtimeRuntime": 0,
            "CpusetCpus": "",
            "CpusetMems": "",
            "Devices": [],
            "DeviceCgroupRules": null,
            "DeviceRequests": null,
            "KernelMemory": 0,
            "KernelMemoryTCP": 0,
            "MemoryReservation": 0,
            "MemorySwap": 0,
            "MemorySwappiness": null,
            "OomKillDisable": false,
            "PidsLimit": null,
            "Ulimits": null,
            "CpuCount": 0,
            "CpuPercent": 0,
            "IOMaximumIOps": 0,
            "IOMaximumBandwidth": 0,
            "MaskedPaths": [
                "/proc/asound",
                "/proc/acpi",
                "/proc/kcore",
                "/proc/keys",
                "/proc/latency_stats",
                "/proc/timer_list",
                "/proc/timer_stats",
                "/proc/sched_debug",
                "/proc/scsi",
                "/sys/firmware"
            ],
            "ReadonlyPaths": [
                "/proc/bus",
                "/proc/fs",
                "/proc/irq",
                "/proc/sys",
                "/proc/sysrq-trigger"
            ]
        },
        "GraphDriver": {
            "Data": {
                "LowerDir": "/var/lib/docker/overlay2/64da1cdab96b6b034078205b942d761cc64d3697641d595feeb6bfff8109795d-init/diff:/var/lib/docker/overlay2/93b1e4d37b158a885e2288d2d7416824e84ec16bcf2803477ccbb6ac626e98bd/diff",
                "MergedDir": "/var/lib/docker/overlay2/64da1cdab96b6b034078205b942d761cc64d3697641d595feeb6bfff8109795d/merged",
                "UpperDir": "/var/lib/docker/overlay2/64da1cdab96b6b034078205b942d761cc64d3697641d595feeb6bfff8109795d/diff",
                "WorkDir": "/var/lib/docker/overlay2/64da1cdab96b6b034078205b942d761cc64d3697641d595feeb6bfff8109795d/work"
            },
            "Name": "overlay2"
        },
        "Mounts": [],
        "Config": {
            "Hostname": "a4ad726a09d4",
            "Domainname": "",
            "User": "",
            "AttachStdin": false,
            "AttachStdout": false,
            "AttachStderr": false,
            "Tty": false,
            "OpenStdin": false,
            "StdinOnce": false,
            "Env": [
                "PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
            ],
            "Cmd": [
                "/bin/sh",
                "-c",
                "while true;do echo kuangshen; sleep 1; done "
            ],
            "Image": "centos",
            "Volumes": null,
            "WorkingDir": "",
            "Entrypoint": null,
            "OnBuild": null,
            "Labels": {
                "org.label-schema.build-date": "20210915",
                "org.label-schema.license": "GPLv2",
                "org.label-schema.name": "CentOS Base Image",
                "org.label-schema.schema-version": "1.0",
                "org.label-schema.vendor": "CentOS"
            }
        },
        "NetworkSettings": {
            "Bridge": "",
            "SandboxID": "85d4b4a888673c8fa0c7af272f20b361429d9d04784cca6ad8fb1363f01ec0e2",
            "HairpinMode": false,
            "LinkLocalIPv6Address": "",
            "LinkLocalIPv6PrefixLen": 0,
            "Ports": {},
            "SandboxKey": "/var/run/docker/netns/85d4b4a88867",
            "SecondaryIPAddresses": null,
            "SecondaryIPv6Addresses": null,
            "EndpointID": "2e660c10d83c1cae559fc736624c5beb2e61dd938ab98ab7f0a00d627be9a4fc",
            "Gateway": "172.17.0.1",
            "GlobalIPv6Address": "",
            "GlobalIPv6PrefixLen": 0,
            "IPAddress": "172.17.0.2",
            "IPPrefixLen": 16,
            "IPv6Gateway": "",
            "MacAddress": "02:42:ac:11:00:02",
            "Networks": {
                "bridge": {
                    "IPAMConfig": null,
                    "Links": null,
                    "Aliases": null,
                    "NetworkID": "cf1034c21237ca530eebfac58bfe33c8f1a0a879bccdd488efb218e0c1e6b5e2",
                    "EndpointID": "2e660c10d83c1cae559fc736624c5beb2e61dd938ab98ab7f0a00d627be9a4fc",
                    "Gateway": "172.17.0.1",
                    "IPAddress": "172.17.0.2",
                    "IPPrefixLen": 16,
                    "IPv6Gateway": "",
                    "GlobalIPv6Address": "",
                    "GlobalIPv6PrefixLen": 0,
                    "MacAddress": "02:42:ac:11:00:02",
                    "DriverOpts": null
                }
            }
        }
    }
]
```

**进入当前正在运行的容器**

```shell
# 进入容器修改配置的命令
# 方式一
docker exec -it 容器id /bin/bash
# 测试
[root@iZuf67gjh27h6gyq7gvprtZ ~]# docker ps
CONTAINER ID   IMAGE     COMMAND                  CREATED       STATUS       PORTS     NAMES
a4ad726a09d4   centos    "/bin/sh -c 'while t…"   3 hours ago   Up 3 hours             serene_maxwell
[root@iZuf67gjh27h6gyq7gvprtZ ~]# 
[root@iZuf67gjh27h6gyq7gvprtZ ~]# 
[root@iZuf67gjh27h6gyq7gvprtZ ~]# docker exec -it a4ad726a09d4 /bin/bash
[root@a4ad726a09d4 /]# ls
bin  dev  etc  home  lib  lib64  lost+found  media  mnt  opt  proc  root  run  sbin  srv  sys  tmp  usr  var
[root@a4ad726a09d4 /]# ps -ef
UID        PID  PPID  C STIME TTY          TIME CMD
root         1     0  0 11:10 ?        00:00:03 /bin/sh -c while true;do echo kuangshen; sleep 1; done 
root     12060     0  0 14:31 pts/0    00:00:00 /bin/bash
root     12087     1  0 14:31 ?        00:00:00 /usr/bin/coreutils --coreutils-prog-shebang=sleep /usr/bin/sleep 1
root     12088 12060  0 14:31 pts/0    00:00:00 ps -ef
[root@a4ad726a09d4 /]# 

# 方式一
docker attach 容器id

# docker exec		# 进入容器后开启一个新的终端，可以在里面操作（常用）
# docker attach		# 进入容器正在执行的终端，不会启动新的进程。
```

**从容器内拷贝文件到主机上**

```shell
docker cp 容器Id:容器内路径 目的的主机路径

# 启动容器并进入
[root@iZuf67gjh27h6gyq7gvprtZ ~]# docker run -it centos /bin/bash
[root@88fdd9828efb /]# ls
bin  dev  etc  home  lib  lib64  lost+found  media  mnt  opt  proc  root  run  sbin  srv  sys  tmp  usr  var
[root@88fdd9828efb /]# cd home
[root@88fdd9828efb home]# ls 
# 创建文件
[root@88fdd9828efb home]# touch Test.java
[root@88fdd9828efb home]# ls
Test.java
# 退出容器
[root@88fdd9828efb home]# exit
exit
#复制文件
[root@iZuf67gjh27h6gyq7gvprtZ ~]# docker ps -a
CONTAINER ID   IMAGE     COMMAND       CREATED         STATUS                      PORTS     NAMES
88fdd9828efb   centos    "/bin/bash"   3 minutes ago   Exited (0) 29 seconds ago             pedantic_elion
[root@iZuf67gjh27h6gyq7gvprtZ ~]# docker cp 88fdd9828efb:/home/Test.java /home
[root@iZuf67gjh27h6gyq7gvprtZ ~]# cd /home
[root@iZuf67gjh27h6gyq7gvprtZ home]# ls
Test.java

# 拷贝是一个手动的过程， 未来我们可以通过 -v 卷的技术实现
```

![命令](.\docker\命令.png)

# 三、实践

## 3.1 部署nginx

```shell
# 搜索nginx镜像
[root@iZuf67gjh27h6gyq7gvprtZ ~]# docker search nginx
NAME                              DESCRIPTION                                     STARS     OFFICIAL   AUTOMATED
nginx                             Official build of Nginx.                        15727     [OK]       
jwilder/nginx-proxy               Automated Nginx reverse proxy for docker con…   2088                 [OK]
richarvey/nginx-php-fpm           Container running Nginx + PHP-FPM capable of…   818                  [OK]
jc21/nginx-proxy-manager          Docker container for managing Nginx proxy ho…   266                  
linuxserver/nginx                 An Nginx container, brought to you by LinuxS…   159                  
jlesage/nginx-proxy-manager       Docker container for Nginx Proxy Manager        143                  [OK]
tiangolo/nginx-rtmp               Docker image with Nginx using the nginx-rtmp…   142                  [OK] 
# 下载nginx镜像不指定版本
[root@iZuf67gjh27h6gyq7gvprtZ ~]# docker pull nginx
Using default tag: latest
latest: Pulling from library/nginx
b380bbd43752: Pull complete 
fca7e12d1754: Pull complete 
745ab57616cb: Pull complete 
a4723e260b6f: Pull complete 
1c84ebdff681: Pull complete 
858292fd2e56: Pull complete 
Digest: sha256:644a70516a26004c97d0d85c7fe1d0c3a67ea8ab7ddf4aff193d9f301670cf36
Status: Downloaded newer image for nginx:latest
docker.io/library/nginx:latest
[root@iZuf67gjh27h6gyq7gvprtZ ~]# docker images
REPOSITORY   TAG       IMAGE ID       CREATED       SIZE
nginx        latest    87a94228f133   2 weeks ago   133MB
centos       latest    5d0da3dc9764   6 weeks ago   231MB
# 启动nginx
# -d 后台启动
# --name 给容器命名
# -p 宿主机端口:容器内部端口
[root@iZuf67gjh27h6gyq7gvprtZ ~]# docker run -d --name nginx01 -p 3304:80 nginx
452a717c6053d9cc8ac74a18019f4c4c90c611c84e484f9a379eaf8ebb52c4ea
[root@iZuf67gjh27h6gyq7gvprtZ ~]# docker ps
CONTAINER ID   IMAGE     COMMAND                  CREATED          STATUS          PORTS                                   NAMES
452a717c6053   nginx     "/docker-entrypoint.…"   13 seconds ago   Up 12 seconds   0.0.0.0:3304->80/tcp, :::3304->80/tcp   nginx01
# 访问
[root@iZuf67gjh27h6gyq7gvprtZ ~]# curl localhost:3344
curl: (7) Failed connect to localhost:3344; Connection refused
[root@iZuf67gjh27h6gyq7gvprtZ ~]# curl localhost:3304
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
html { color-scheme: light dark; }
body { width: 35em; margin: 0 auto;
font-family: Tahoma, Verdana, Arial, sans-serif; }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>

# 进入容器
[root@iZuf67gjh27h6gyq7gvprtZ ~]# docker exec -it nginx01 /bin/bash 
root@452a717c6053:/# whereis nginx
nginx: /usr/sbin/nginx /usr/lib/nginx /etc/nginx /usr/share/nginx
root@452a717c6053:/# cd /etc/nginx/
root@452a717c6053:/etc/nginx# ls
conf.d	fastcgi_params	mime.types  modules  nginx.conf  scgi_params  uwsgi_params
```

## 3.2 部署tomcat

```shell
# 部署tomcat
docker run -it --rm tomcat:9.0
# 我们一般用的都是后台启动，停止了容器之后还是可以查到， docker run -it --rm tomcat:9.0 一般用来测试，用完就删除
 
# 下载再启动
# 下载镜像
[root@iZuf67gjh27h6gyq7gvprtZ ~]# docker pull tomcat
Using default tag: latest
latest: Pulling from library/tomcat
bb7d5a84853b: Already exists 
f02b617c6a8c: Already exists 
d32e17419b7e: Already exists 
c9d2d81226a4: Already exists 
fab4960f9cd2: Already exists 
da1c1e7baf6d: Already exists 
1d2ade66c57e: Already exists 
ea2ad3f7cb7c: Already exists 
d75cb8d0a5ae: Pull complete 
76c37a4fffe6: Pull complete 
Digest: sha256:509cf786b26a8bd43e58a90beba60bdfd6927d2ce9c7902cfa675d3ea9f4c631
Status: Downloaded newer image for tomcat:latest
docker.io/library/tomcat:latest
[root@iZuf67gjh27h6gyq7gvprtZ ~]# docker images
REPOSITORY   TAG       IMAGE ID       CREATED       SIZE
tomcat       9.0       43e421a14aec   9 days ago    680MB
tomcat       latest    b0e0b0a92cf9   9 days ago    680MB
nginx        latest    87a94228f133   2 weeks ago   133MB
centos       latest    5d0da3dc9764   6 weeks ago   231MB
# 启动容器
[root@iZuf67gjh27h6gyq7gvprtZ ~]# docker run -d -p 8033:8080  --name tomcat01 tomcat 
6f27cacf34659d2358222de4adb39aac6ccc33a4a173abc4b09f58d4c591afd9
# 进入容器
[root@iZuf67gjh27h6gyq7gvprtZ ~]# docker exec -it tomcat01 /bin/bash
root@6f27cacf3465:/usr/local/tomcat# ls
BUILDING.txt  CONTRIBUTING.md  LICENSE	NOTICE	README.md  RELEASE-NOTES  RUNNING.txt  bin  conf  lib  logs  native-jni-lib  temp  webapps  webapps.dist  work
root@6f27cacf3465:/usr/local/tomcat# ls
BUILDING.txt  CONTRIBUTING.md  LICENSE	NOTICE	README.md  RELEASE-NOTES  RUNNING.txt  bin  conf  lib  logs  native-jni-lib  temp  webapps  webapps.dist  work
# 复制toncat静态资源
root@6f27cacf3465:/usr/local/tomcat# cp -r  webapps.dist/* webapps
root@6f27cacf3465:/usr/local/tomcat# cd webapps
root@6f27cacf3465:/usr/local/tomcat/webapps# ls
ROOT  docs  examples  host-manager  manager

# 发现问题 1.linux命令少了 2.webapps没有静态资源。	阿里云镜像的原因。默认的是最小镜像，所以不必要的都剔除掉。保证最小可运行环境   

```

## 3.3 部署es+kibana

``` shell
# es暴露的端口很多
# es十分耗内存
# es的数据一般要防止到安全目录！挂载
# --net somenetwork？网络配置
docker run -d --name elasticsearch -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" elasticsearch:7.6.2
[root@iZuf67gjh27h6gyq7gvprtZ ~]# docker run -d --name elasticsearch -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" elasticsearch:7.6.2
111743b15e4c39bfff8d46c08b1347e382cbe93b0be7576bf341718dfacf1bc5

# 验证
[root@iZuf67gjh27h6gyq7gvprtZ ~]# curl localhost:9200
{
  "name" : "111743b15e4c",
  "cluster_name" : "docker-cluster",
  "cluster_uuid" : "rZ5QPy6GRo-oR5jsb3gBwg",
  "version" : {
    "number" : "7.6.2",
    "build_flavor" : "default",
    "build_type" : "docker",
    "build_hash" : "ef48eb35cf30adf4db14086e8aabd07ef6fb113f",
    "build_date" : "2020-03-26T06:34:37.794943Z",
    "build_snapshot" : false,
    "lucene_version" : "8.4.0",
    "minimum_wire_compatibility_version" : "6.8.0",
    "minimum_index_compatibility_version" : "6.0.0-beta1"
  },
  "tagline" : "You Know, for Search"
}

# docker state 查看CPU状态
```

![image-20211031191832923](.\DOCKER\image-20211031191832923.png)

```shell
# 非常耗内存，关闭之后增加内存限制
docker run -d --name elasticsearch -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" -e ES_JAVA_OPTS="-Xms64m -Xmx512m" elasticsearch:7.6.2

# 测试
[root@iZuf67gjh27h6gyq7gvprtZ ~]# docker run -d --name elasticsearch01 -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" -e ES_JAVA_OPTS="-Xms64m -Xmx512m" elasticsearch:7.6.2
f98918f05d75d1c24a24b1b54824f1028e9354a88340a1ed263122d89226db95
[root@iZuf67gjh27h6gyq7gvprtZ ~]# curl localhost:9200
{
  "name" : "f98918f05d75",
  "cluster_name" : "docker-cluster",
  "cluster_uuid" : "G8nNxMp-Sxydq-tPG-DNrg",
  "version" : {
    "number" : "7.6.2",
    "build_flavor" : "default",
    "build_type" : "docker",
    "build_hash" : "ef48eb35cf30adf4db14086e8aabd07ef6fb113f",
    "build_date" : "2020-03-26T06:34:37.794943Z",
    "build_snapshot" : false,
    "lucene_version" : "8.4.0",
    "minimum_wire_compatibility_version" : "6.8.0",
    "minimum_index_compatibility_version" : "6.0.0-beta1"
  },
  "tagline" : "You Know, for Search"
}
[root@iZuf67gjh27h6gyq7gvprtZ ~]# docker stats
```

![image-20211031193824078](.\docker\image-20211031193824078.png)

# 四、可视化界面

- portainer(先用这个)

```shell
docker run -d -p 8088:9000 \
--restart=always -v /var/run/docker.sock:/var/run/docker.sock --privileged=true portainer/portainer

admin /fendaJava
```

- Rancher（CI/CD再用）

![image-20211031201432276](.\docker\image-20211031201432276.png)

# 五、Docker镜像讲解

## 5.1、镜像是什么

镜像是一种轻量级、可执行的独立软件包，用来打包软件运行环境和基于运行环境开发的软件，它包含运行某个软件所需的所有内容，包括代码、运行时、库、环境变量和配置文件。所有的应用，直接打包docker镜像，就可以直接跑起来!
如何得到镜像:

- 从远程仓库下载
- 朋友拷贝给你
- 自己制作一个镜像 DockerFile

## 5.2、Docker镜像加载原理

> UnionFS(联合文件系统)

我们下载镜像的时候一层层的就是这个！

UnionFS(联合文件系统)：Union文件系统(UnionFS)是一种分层、轻量级并且高性能的文件系统，它支持对文件系统的修改作为一次提交来一层层的叠加，同时可以将不同目录挂载到同一个虚拟文件系统下(unite several directories into a single virtual filesystem)。Union 文件系统是 Docker 镜像的基础。镜像可以通过分层来进行继承，基于基础镜像(没有父镜像)，可以制作各种具体的应用镜像。
特性：一次同时加载多个文件系统，但从外面看起来，只能看到一个文件系统，联合加载会把各层文件系统叠加起来，这样最终的文件系统会包含所有底层的文件和目录。

> Docker镜像加载原理

docker的镜像实际上由一层一层的文件系统组成，这种层级的文件系统UnionFS。
bootfs(boot file system)主要包含bootloader和kernel,bootloader主要是引导加载kernel,Linux刚启动时会加载bootfs文件系统，在Docker镜像的最底层是bootfs。这一层与我们典型的Linux/Unix系统是一样的，包含boot加载器和内核。当boot加载完成之后整个内核就都在内存中了，此时内存的使用权已由bootfs转交给内核，此时系统也会卸载bootfs。

rootfs(root file system)，在bootfs之上。包含的就是典型Linux 系统中的/dev./proc,/bin./etc 等标准目录和文件。rootfs就是各种不同的操作系统发行版，比如Ubuntu，Centos等等。

![image-20211031203604732](.\docker\image-20211031203604732.png)

平时我们安装虚拟机的CentOS都是好几G，为什么docker的两百兆

对于一个精简的OS，rootfs 可以很小，只需要包含最基本的命令，工具和程序库就可以了，因为底层直接用Host的kernel，自己只需要提供rootfs就可以了，由此可见对于不同的linux发行版,bootfs基本是一致的rodtfs会有差别,因此不同的发行版可以公用 bootfs。

**虚拟机是分钟级别，容器是秒级!**

## 5.3 分层的理解

> 分层的镜像

我们可以下载一个镜像，注意观察下载的日志输出，可以看到一层一层的在下载。

![image-20211031205047432](.\docker\image-20211031205047432.png)

思考:为什么Docker镜像要采用这种分层的结构呢?

最大的好处，我觉得莫过于是资源共享了!比如有多个镜像都从相同的Base镜像构建而来，那么宿主机只需在磁盘上保留一份base镜像，同时内存中也只需要加载一份base镜像，这样就可以为所有的容器服务了，而且镜像的每一层都可以被共享。

查看镜像分层的方式可以通过 docker image inspect 命令!

```shell
[root@iZuf67gjh27h6gyq7gvprtZ /]# docker image inspect redis:latest
[
    {
        "Id": "sha256:7faaec68323851b2265bddb239bd9476c7d4e4335e9fd88cbfcc1df374dded2f",
        "RepoTags": [
            "redis:latest"
        ],
        "RepoDigests": [
            "redis@sha256:a89cb097693dd354de598d279c304a1c73ee550fbfff6d9ee515568e0c749cfe"
        ],
        "Parent": "",
        "Comment": "",
        "Created": "2021-10-12T09:42:28.403526293Z",
        "Container": "10b9267d91b3a0b3d03e429697cf6edda84ecb6447d5c4a5d6054d8ed189c71a",
        "ContainerConfig": {
            "Hostname": "10b9267d91b3",
            "Domainname": "",
            "User": "",
            "AttachStdin": false,
            "AttachStdout": false,
            "AttachStderr": false,
            "ExposedPorts": {
                "6379/tcp": {}
            },
            "Tty": false,
            "OpenStdin": false,
            "StdinOnce": false,
            "Env": [
                "PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
                "GOSU_VERSION=1.12",
                "REDIS_VERSION=6.2.6",
                "REDIS_DOWNLOAD_URL=http://download.redis.io/releases/redis-6.2.6.tar.gz",
                "REDIS_DOWNLOAD_SHA=5b2b8b7a50111ef395bf1c1d5be11e6e167ac018125055daa8b5c2317ae131ab"
            ],
            "Cmd": [
                "/bin/sh",
                "-c",
                "#(nop) ",
                "CMD [\"redis-server\"]"
            ],
            "Image": "sha256:77d41858d117f5fe9255dbff7861254660bb6789f3a74f69eebf0d432c214a52",
            "Volumes": {
                "/data": {}
            },
            "WorkingDir": "/data",
            "Entrypoint": [
                "docker-entrypoint.sh"
            ],
            "OnBuild": null,
            "Labels": {}
        },
        "DockerVersion": "20.10.7",
        "Author": "",
        "Config": {
            "Hostname": "",
            "Domainname": "",
            "User": "",
            "AttachStdin": false,
            "AttachStdout": false,
            "AttachStderr": false,
            "ExposedPorts": {
                "6379/tcp": {}
            },
            "Tty": false,
            "OpenStdin": false,
            "StdinOnce": false,
            "Env": [
                "PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
                "GOSU_VERSION=1.12",
                "REDIS_VERSION=6.2.6",
                "REDIS_DOWNLOAD_URL=http://download.redis.io/releases/redis-6.2.6.tar.gz",
                "REDIS_DOWNLOAD_SHA=5b2b8b7a50111ef395bf1c1d5be11e6e167ac018125055daa8b5c2317ae131ab"
            ],
            "Cmd": [
                "redis-server"
            ],
            "Image": "sha256:77d41858d117f5fe9255dbff7861254660bb6789f3a74f69eebf0d432c214a52",
            "Volumes": {
                "/data": {}
            },
            "WorkingDir": "/data",
            "Entrypoint": [
                "docker-entrypoint.sh"
            ],
            "OnBuild": null,
            "Labels": null
        },
        "Architecture": "amd64",
        "Os": "linux",
        "Size": 112691012,
        "VirtualSize": 112691012,
        "GraphDriver": {
            "Data": {
                "LowerDir": "/var/lib/docker/overlay2/b53824a521d3046861902318d6f3a7e2c111f5ca2cbe8b0621d64a88fead2d9a/diff:/var/lib/docker/overlay2/d8168bc357859ee049b684a6363aec62d7888db6a675a8d95e1fae239fa5d43b/diff:/var/lib/docker/overlay2/ee8725f697ae2e85088cb632bb0b13761cb126d31a4a4893b55dea7bd5cda29c/diff:/var/lib/docker/overlay2/a64a93d14f8802230f8d5bb29c62beedf137c66fbed358ad1520c3645f24ce9f/diff:/var/lib/docker/overlay2/f52fff4e682d935c0360c1b55118783e2b904471cd1d3f5f69154e6f2fb4ab8f/diff",
                "MergedDir": "/var/lib/docker/overlay2/a283aee66bd8e44dc995c636af245b1220cba05fbba98cfbcc4d2fdeedd9f04e/merged",
                "UpperDir": "/var/lib/docker/overlay2/a283aee66bd8e44dc995c636af245b1220cba05fbba98cfbcc4d2fdeedd9f04e/diff",
                "WorkDir": "/var/lib/docker/overlay2/a283aee66bd8e44dc995c636af245b1220cba05fbba98cfbcc4d2fdeedd9f04e/work"
            },
            "Name": "overlay2"
        },
        "RootFS": {
            "Type": "layers",
            "Layers": [
                "sha256:e8b689711f21f9301c40bf2131ce1a1905c3aa09def1de5ec43cf0adf652576e",
                "sha256:b43651130521eb89ffc3234909373dc42557557b3a6609b9fed183abaa0c4085",
                "sha256:8b9770153666c1eef1bc685abfc407242d31e34f180ad0e36aff1a7feaeb3d9c",
                "sha256:6b01cc47a390133785a4dd0d161de0cb333fe72e541d1618829353410c4facef",
                "sha256:0bd13b42de4de0a0d0cc3f1f162cd0d4b8cb4ee20cbea7302164fdc6894955fd",
                "sha256:146262eb38412d6eb44be1710bfe0f05d3493831f82b1c2be8dc8d9558c9f033"
            ]
        },
        "Metadata": {
            "LastTagTime": "0001-01-01T00:00:00Z"
        }
    }
]
```

**理解:**
所有的 Docker 镜像都起始于一个基础镜像层，当进行修改或增加新的内容时，就会在当前镜像层之上，创建新的镜像层。
举一个简单的例子，假如基于 Ubuntu Linux 16.04 创建一个新的镜像，这就是新镜像的第一层;如果在该镜像中添加 Python包，就会在基础镜像层之上创建第二个镜像层;如果继续添加一个安全补丁，就会创建第三个镜像层。
该镜像当前已经包含 3个镜像层，如下图所示(这只是一个用于演示的很简单的例子)。

![image-20211031205722678](.\docker\image-20211031205722678.png)

在添加额外的镜像层的同时，镜像始终保持是当前所有镜像的组合，理解这一点非常重要。下图中举了一个简单的例子，每个镜像层包含3个文件，而镜像包含了来自两个镜像层的6个文件。

![image-20211031205946127](.\docker\image-20211031205946127.png)

上图中的镜像展示跟之前的略有不同，主要目的便于展示文件。

下图展示了一个较为复制的三层镜像，在外部看来整个镜像只有六个文件，这是因为最上层的文件7是文件5的更新版本。

![image-20211031210257288](.\docker\image-20211031210257288.png)

这种情况下，上层镜像层中的文件覆盖了底层镜像层中的文件。这样就使得文件的更新版本作为一个新镜像层添加到镜像当中。
Docker通过存储引擎(新版本采用快照机制)的方式来实现镜像层堆栈，并保证多镜像层对外展示为统一的文件系统。
Linux上可用的存储引擎有 AUFS、Overlay2、Device Mapper、Btrfs 以及ZFS。顾名思义，每种存储引擎都基于 Linux 中对应的文件系统或者块设备技术，并且每种存储引擎都有其独有的性能特点。
Docker在 Windows 上仅支持 windowsfilter一种存储引擎，该引擎基于 NTFS 文件系统之上实现了分层和 CoW[1]。
下图展示了与系统显示相同的三层镜像。所有镜像层堆叠并合并，对外提供统一的视图。

![image-20211031211008177](.\docker\image-20211031211008177.png)

> 特点

Docker镜像都是只读的，当容器启动时，一个新的可写层被加载到镜像的顶部!
这一层就是我们通常说的容器层，容器之下的都叫镜像层!

## 5.4 commit镜像

如果想保存一个容器的状态，就可以提交通过commit提交，来获取一个镜像

就好比VM的镜像

```shell
docker commit 提交容器成为一个新的版本

# 命令和git原理相似
docker commit -m="提交的描述信息" -a="作者" 镜像id 目标镜像名：[TAG]
```

实战测试

```shell
# 1、启动一个默认的tomcat
[root@iZuf67gjh27h6gyq7gvprtZ /]# docker run -d -p 8080:8080 --name tomcat tomcat
61470c808b445c20f49ea0ec735c8201a8fb304fd10e9a5dbe1c5de0ec16f1bb
[root@iZuf67gjh27h6gyq7gvprtZ /]# docker ps
CONTAINER ID   IMAGE                 COMMAND             CREATED         STATUS         PORTS                                       NAMES
61470c808b44   tomcat                "catalina.sh run"   5 seconds ago   Up 5 seconds   0.0.0.0:8080->8080/tcp, :::8080->8080/tcp   tomcat
1b7717fc1222   portainer/portainer   "/portainer"        3 hours ago     Up 3 hours     0.0.0.0:8088->9000/tcp, :::8088->9000/tcp   laughing_mahavira

# 2、发现这个默然的tomcat，是没有webapps应用。镜像的原因，官方的镜像默认webapps下是没有文件的！
# 3、自己复制进去基本的文件
[root@iZuf67gjh27h6gyq7gvprtZ /]# docker exec -it 61470c808b44 /bin/bash
root@61470c808b44:/usr/local/tomcat# ls
BUILDING.txt  CONTRIBUTING.md  LICENSE	NOTICE	README.md  RELEASE-NOTES  RUNNING.txt  bin  conf  lib  logs  native-jni-lib  temp  webapps  webapps.dist  work
root@61470c808b44:/usr/local/tomcat# cd webapps
root@61470c808b44:/usr/local/tomcat/webapps# ls
root@61470c808b44:/usr/local/tomcat/webapps# cd ../
root@61470c808b44:/usr/local/tomcat# ls
BUILDING.txt  CONTRIBUTING.md  LICENSE	NOTICE	README.md  RELEASE-NOTES  RUNNING.txt  bin  conf  lib  logs  native-jni-lib  temp  webapps  webapps.dist  work
root@61470c808b44:/usr/local/tomcat# cp -r webapps.dist/* webapps
root@61470c808b44:/usr/local/tomcat# cd webapps
root@61470c808b44:/usr/local/tomcat/webapps# ls
ROOT  docs  examples  host-manager  manager
root@61470c808b44:/usr/local/tomcat/webapps# exit
exit
[root@iZuf67gjh27h6gyq7gvprtZ /]# docker ps
CONTAINER ID   IMAGE                 COMMAND             CREATED         STATUS         PORTS                                       NAMES
61470c808b44   tomcat                "catalina.sh run"   4 minutes ago   Up 4 minutes   0.0.0.0:8080->8080/tcp, :::8080->8080/tcp   tomcat
1b7717fc1222   portainer/portainer   "/portainer"        3 hours ago     Up 3 hours     0.0.0.0:8088->9000/tcp, :::8088->9000/tcp   laughing_mahavira

# 4、将我们操作过的容器通过commit提交为一个镜像！以后使用修改过的镜像即可
[root@iZuf67gjh27h6gyq7gvprtZ /]# docker commit -a="liuhang" -m="add webapps ap" 61470c808b44 tomcat:1.0
sha256:cdf9a064371f376dcb4bad57b204f0a9fbe05128a658c3c079f8e4d14363feb4
[root@iZuf67gjh27h6gyq7gvprtZ /]# docker images
REPOSITORY            TAG       IMAGE ID       CREATED          SIZE
tomcat                1.0       cdf9a064371f   13 seconds ago   684MB
tomcat                9.0       43e421a14aec   9 days ago       680MB
tomcat                latest    b0e0b0a92cf9   9 days ago       680MB
redis                 latest    7faaec683238   2 weeks ago      113MB
nginx                 latest    87a94228f133   2 weeks ago      133MB
centos                latest    5d0da3dc9764   6 weeks ago      231MB
elasticsearch         7.14.2    2abd5342ace0   6 weeks ago      1.04GB
portainer/portainer   latest    580c0e4e98b0   7 months ago     79.1MB
elasticsearch         7.6.2     f29a1ee41030   19 months ago    791MB
[root@iZuf67gjh27h6gyq7gvprtZ /]# 
```

# 六、容器数据卷

## 6.1 什么是容器数据卷

容器的数据是保存在容器内的，当删除容器时对应的应用数据也就删除了。对应一些我们希望保存起来的数据，这样是不方便稳定的，比如mysql的数据文件。

容器数据卷就是容器于容器之间，容器与宿主机机之间的一种文件同步共享的技术。可以将容器的目录挂载到宿主机的目录上。

**总结：容器的持久化和同步操作，容器间也是可以实现数据共享的。**

## 6.2 使用数据卷

> 方式一：直接使用命令来挂载 -v

``` shell
docker run -it -v 主机目录:容器内目录 
```

![image-20211031234406508](.\docker\image-20211031234406508.png)

先停掉容器，在宿主机修改文件也能同步到容器

![image-20211031235355523](.\docker\image-20211031235355523.png)

**好处：添加数据卷后我们可以直接修改宿主机文件**

## 6.3 mysql安装

```shell
# 查询镜像
[root@iZuf67gjh27h6gyq7gvprtZ ~]# docker search mysql
NAME                              DESCRIPTION                                     STARS     OFFICIAL   AUTOMATED
mysql                             MySQL is a widely used, open-source relation…   11803     [OK]       
mariadb                           MariaDB Server is a high performing open sou…   4492      [OK]       
# 下载镜像
[root@iZuf67gjh27h6gyq7gvprtZ ~]# docker pull mysql:5.7
5.7: Pulling from library/mysql
ffbb094f4f9e: Pull complete 
df186527fc46: Pull complete 
fa362a6aa7bd: Pull complete 
5af7cb1a200e: Pull complete 
949da226cc6d: Pull complete 
bce007079ee9: Pull complete 
eab9f076e5a3: Pull complete 
c7b24c3f27af: Pull complete 
6fc26ff6705a: Pull complete 
bec5cdb5e7f7: Pull complete 
6c1cb25f7525: Pull complete 
Digest: sha256:d1cc87a3bd5dc07defc837bc9084f748a130606ff41923f46dec1986e0dc828d
Status: Downloaded newer image for mysql:5.7
docker.io/library/mysql:5.7
[root@iZuf67gjh27h6gyq7gvprtZ ~]# docker images
REPOSITORY            TAG       IMAGE ID       CREATED         SIZE
mysql                 5.7       738e7101490b   12 days ago     448MB
# 启动mysql
# -d 后台启动
# -p 指定映射端口
# -v 指定挂载卷
# -e 指定参数
# --name 指定容器名称
[root@iZuf67gjh27h6gyq7gvprtZ ~]# docker run -d -p 3310:3306 -v /home/mysql/conf:/etc/mysql/conf.d -v /home/mysql/data:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=123456 --name mysql01 mysql:5.7
87662f953a4758a3f4ec6e40bc9466e5a649491eabe00087c7a3e5c5d1b04c6d
[root@iZuf67gjh27h6gyq7gvprtZ ~]# cd /home
[root@iZuf67gjh27h6gyq7gvprtZ home]# ll
total 8
drwxr-xr-x 4 root root 4096 Dec 14 21:45 mysql
drwxr-xr-x 2 root root 4096 Oct 31 23:47 test
-rw-r--r-- 1 root root    0 Oct 30 22:51 Test.java
[root@iZuf67gjh27h6gyq7gvprtZ home]# cd mysql/
[root@iZuf67gjh27h6gyq7gvprtZ mysql]# ll
total 8
drwxr-xr-x 2 root    root 4096 Dec 14 21:45 conf
drwxr-xr-x 5 polkitd root 4096 Dec 14 21:45 data
[root@iZuf67gjh27h6gyq7gvprtZ mysql]# cd data/
[root@iZuf67gjh27h6gyq7gvprtZ data]# ll
total 188476
-rw-r----- 1 polkitd input       56 Dec 14 21:45 auto.cnf
-rw------- 1 polkitd input     1680 Dec 14 21:45 ca-key.pem
-rw-r--r-- 1 polkitd input     1112 Dec 14 21:45 ca.pem
-rw-r--r-- 1 polkitd input     1112 Dec 14 21:45 client-cert.pem
-rw------- 1 polkitd input     1676 Dec 14 21:45 client-key.pem
-rw-r----- 1 polkitd input     1352 Dec 14 21:45 ib_buffer_pool
-rw-r----- 1 polkitd input 79691776 Dec 14 21:45 ibdata1
-rw-r----- 1 polkitd input 50331648 Dec 14 21:45 ib_logfile0
-rw-r----- 1 polkitd input 50331648 Dec 14 21:45 ib_logfile1
-rw-r----- 1 polkitd input 12582912 Dec 14 21:45 ibtmp1
drwxr-x--- 2 polkitd input     4096 Dec 14 21:45 mysql
drwxr-x--- 2 polkitd input     4096 Dec 14 21:45 performance_schema
-rw------- 1 polkitd input     1676 Dec 14 21:45 private_key.pem
-rw-r--r-- 1 polkitd input      452 Dec 14 21:45 public_key.pem
-rw-r--r-- 1 polkitd input     1112 Dec 14 21:45 server-cert.pem
-rw------- 1 polkitd input     1680 Dec 14 21:45 server-key.pem
drwxr-x--- 2 polkitd input    12288 Dec 14 21:45 sys
[root@iZuf67gjh27h6gyq7gvprtZ data]# 

# 删除容器，发现挂载到本地的数据卷依旧没有丢失
[root@iZuf67gjh27h6gyq7gvprtZ data]# docker rm -f mysql01
mysql01
[root@iZuf67gjh27h6gyq7gvprtZ data]# docker ps
CONTAINER ID   IMAGE                 COMMAND        CREATED       STATUS       PORTS                                       NAMES
1b7717fc1222   portainer/portainer   "/portainer"   6 weeks ago   Up 6 weeks   0.0.0.0:8088->9000/tcp, :::8088->9000/tcp   laughing_mahavira
[root@iZuf67gjh27h6gyq7gvprtZ data]#  docker ps -a
CONTAINER ID   IMAGE                 COMMAND                  CREATED       STATUS                     PORTS                                       NAMES
1b7717fc1222   portainer/portainer   "/portainer"             6 weeks ago   Up 6 weeks                 0.0.0.0:8088->9000/tcp, :::8088->9000/tcp   laughing_mahavira
[root@iZuf67gjh27h6gyq7gvprtZ data]# ll
total 188476
-rw-r----- 1 polkitd input       56 Dec 14 21:45 auto.cnf
-rw------- 1 polkitd input     1680 Dec 14 21:45 ca-key.pem
-rw-r--r-- 1 polkitd input     1112 Dec 14 21:45 ca.pem
-rw-r--r-- 1 polkitd input     1112 Dec 14 21:45 client-cert.pem
-rw------- 1 polkitd input     1676 Dec 14 21:45 client-key.pem
-rw-r----- 1 polkitd input     1352 Dec 14 21:45 ib_buffer_pool
-rw-r----- 1 polkitd input 79691776 Dec 14 21:45 ibdata1
-rw-r----- 1 polkitd input 50331648 Dec 14 21:45 ib_logfile0
-rw-r----- 1 polkitd input 50331648 Dec 14 21:45 ib_logfile1
-rw-r----- 1 polkitd input 12582912 Dec 14 21:45 ibtmp1
drwxr-x--- 2 polkitd input     4096 Dec 14 21:45 mysql
drwxr-x--- 2 polkitd input     4096 Dec 14 21:45 performance_schema
-rw------- 1 polkitd input     1676 Dec 14 21:45 private_key.pem
-rw-r--r-- 1 polkitd input      452 Dec 14 21:45 public_key.pem
-rw-r--r-- 1 polkitd input     1112 Dec 14 21:45 server-cert.pem
-rw------- 1 polkitd input     1680 Dec 14 21:45 server-key.pem
drwxr-x--- 2 polkitd input    12288 Dec 14 21:45 sys
```

## 6.4 具名和匿名挂载

```shell
# 匿名挂载
# -v 容器内路径！
# docker run -d -p --name nginx01 -v /etc/nginx nginx

[root@iZuf67gjh27h6gyq7gvprtZ data]# docker run -d -P --name nginx03 -v /etc/nginx nginx
225a43220b8f44405aabc9da3e983b2decd8ae0f98a04f713b5ee1cdfa0f0bdd
[root@iZuf67gjh27h6gyq7gvprtZ data]# docker volume ls
DRIVER    VOLUME NAME
local     d7106a384921b90581fd88bfb0524f7bb1c115a0fb99dec6e180d8a4ec788219

# 具名挂载
# -v 卷名:容器内路径名
[root@iZuf67gjh27h6gyq7gvprtZ data]# docker run -d -P --name nginx04 -v juming-nginx:/etc/nginx nginx
482118e8684f733b050cc90d72c366dc69e638b5d27ef747a4662b9cb4df87db
[root@iZuf67gjh27h6gyq7gvprtZ data]# docker volume ls
DRIVER    VOLUME NAME
local     juming-nginx
[root@iZuf67gjh27h6gyq7gvprtZ data]# docker volume inspect juming-nginx
[
    {
        "CreatedAt": "2021-12-14T22:36:03+08:00",
        "Driver": "local",
        "Labels": null,
        "Mountpoint": "/var/lib/docker/volumes/juming-nginx/_data",
        "Name": "juming-nginx",
        "Options": null,
        "Scope": "local"
    }
]
[root@iZuf67gjh27h6gyq7gvprtZ data]# cd /var/lib/docker/volumes/juming-nginx/_data
[root@iZuf67gjh27h6gyq7gvprtZ _data]# ls
conf.d  fastcgi_params  mime.types  modules  nginx.conf  scgi_params  uwsgi_params
[root@iZuf67gjh27h6gyq7gvprtZ _data]# cd ..
[root@iZuf67gjh27h6gyq7gvprtZ juming-nginx]# ls
_data
[root@iZuf67gjh27h6gyq7gvprtZ juming-nginx]# cd ..
[root@iZuf67gjh27h6gyq7gvprtZ volumes]# ll
total 36
drwx-----x 3 root root   4096 Oct 31 20:01 314786e7d601dc0858a2c65cf74072449476f1f8573ae612721bf137eb1539d8
brw------- 1 root root 253, 1 Oct 18 22:52 backingFsBlockDev
drwx-----x 3 root root   4096 Dec 14 22:34 d7106a384921b90581fd88bfb0524f7bb1c115a0fb99dec6e180d8a4ec788219
drwx-----x 3 root root   4096 Dec 14 22:36 juming-nginx
-rw------- 1 root root  32768 Dec 14 22:36 metadata.db
```

所有的docker容器内的卷，没有指定目录的情况下都是在 /var/lib/docker/volumes/xxxx/_data

我们通过具名挂载可以方便的找到我们的一个卷，大多数情况下使用具名挂载

```shell
# 挂载类型
-v 容器内路径				# 匿名挂载
-v 卷名:容器内路径		   		# 具名挂载
-v /宿主机路径:容器内路径			# 指定路径挂载

# 拓展
# 通过 -v 容器内路径: ro rw 改变读写权限
ro readonly # 只读 只能通过宿主机来修改 ，容器内无法修改
rw readwrite # 可读可写
docker run -d -P --name nginx03 -v /etc/nginx:ro nginx
docker run -d -P --name nginx03 -v /etc/nginx:rw nginx
```

## 6.5 初始DockerFile

Dockerfile就是构建 docker 镜像的构建文件！命令脚本！

通过这个脚本可以生成镜像，镜像是一层一层的，脚本一个一个命令，每个命令就是一层。

```shell
# 创建一个dockerfile文件名字可以随机 建议Dockerfile
# 文件中的内容 指令(大写) 参数
FROM centos

VOLUME ["volume01","volume02"]

CMD echo "------------end----------------"
CMD /bin/bash

# 构建镜像
[root@iZuf67gjh27h6gyq7gvprtZ docker-test-volume]# docker build -f /home/docker-test-volume/dockerFile1 -t liuhang/centos:1.0 .
Sending build context to Docker daemon  2.048kB
Step 1/4 : FROM centos
 ---> 5d0da3dc9764
Step 2/4 : VOLUME ["volume01","volume02"]
 ---> Running in ee33d38196de
Removing intermediate container ee33d38196de
 ---> 8da19eca8144
Step 3/4 : CMD echo "------------end----------------"
 ---> Running in 78fde128c90d
Removing intermediate container 78fde128c90d
 ---> 62c1ad8f2b3b
Step 4/4 : CMD /bin/bash
 ---> Running in 060425dca247
Removing intermediate container 060425dca247
 ---> 7d6f62b954d5
Successfully built 7d6f62b954d5
Successfully tagged liuhang/centos:1.0
# 查看镜像
[root@iZuf67gjh27h6gyq7gvprtZ docker-test-volume]# docker images
REPOSITORY            TAG       IMAGE ID       CREATED          SIZE
liuhang/centos        1.0       7d6f62b954d5   28 seconds ago   231MB
[root@iZuf67gjh27h6gyq7gvprtZ docker-test-volume]# 

```

![4c53fe74bfa41da611a920f5ea76de3](.\docker\4c53fe74bfa41da611a920f5ea76de3.png)

宿主机地址文件地址

![4c53fe74bfa41da611a920f5ea76de3](.\docker\344b2c0837a0f547ffb3c6830ffd2fd.png)

# 七、DockerFile

## 7.1 DockerFile介绍

dockerFile 是用来构建docker镜像的文件！命令行文件。

构建步骤：

1、编写一个dockerFile文件。

2、docker build 构建成为一个镜像

3、docker run 运行镜像

4、docker push 发布镜像（DockerHub,阿里云镜像仓库）

## 7.2 DockerFile构建过程

**基础知识：**

1、每个保留关键字（指令）都必须是大写

2、执行从上到下顺序

3、# 表示注释

4、每一个指令都会创建提交一个新的镜像层，并提交！ 

![094c7b15eed161f786a5df8fed5837f](.\docker\094c7b15eed161f786a5df8fed5837f.png)

## 7.3 DockerFile指令

```shell
FROM             # 基础镜像，一切从这里开始构建
MAINTAINER       # 镜像是谁写的，姓名+邮箱
RUN              # 镜像构建的时候需要运行的命令
ADD              # 步骤，tomcat镜像，这个tomcat压缩包！添加内容
WORKDIR          # 镜像的工作目录
VOLUM            # 挂载的目录
EXPOST           # 暴露端口
CMD              # 指定容器启动时候要执行的命令,只有最后一个会生效，可被替代
ENTRYPOIT        # 指定容器启动时候要执行的命令，可以追加命令
ONBUILD          # 当构建一个被继承的 DockeFile这个时候就会运行 ONBUILD 的指令。触发指令
COPY             # 类似ADD，将我们文件拷贝到镜像中
ENV              # 构建的时候设值环境变量
```



![1639828501397](./docker/1639828501397.png)

![094c7b15eed161f786a5df8fed5837f](.\docker\8ccbcd459b37a9fda3af5eebc186a1f.png)

## 7.4 实战测试

Docker Hub 中99%镜像都是从这个基础镜像过来的 FROM scratch ,然后配置需要的软件和配置来进行的构建

![1639828501397](./docker/36a1eb7a12b6e50ca3ce7053b1f0a4b.png)

### 创建一个自己的centos

```shell
# 1.编写dockerfile文件
[root@iZuf67gjh27h6gyq7gvprtZ dockerfile]# vim dockerfile-centos
[root@iZuf67gjh27h6gyq7gvprtZ dockerfile]# cat dockerfile-centos 
FROM centos 
MAINTAINER liuhang<435354105.qq.com>

ENV MYPATH /usr/local
WORKDIR $MYPATH

RUN yum -y install vim
RUN yum -y install net-tools

EXPOSE 80
CMD echo $MYPATH
CMD echo "--------end--------"
CMD /bin/bash
# 2、通过这个文件构建镜像
# 命令 docker build -f dockerfile文件路径 -t 镜像名:[tag]
[root@iZuf67gjh27h6gyq7gvprtZ dockerfile]# docker build -f dockerfile-centos -t mycentos:0.1 .
Sending build context to Docker daemon  2.048kB
Step 1/10 : FROM centos
 ---> 5d0da3dc9764
Step 2/10 : MAINTAINER liuhang<435354105.qq.com>
 ---> Running in e9b6636ca5fc
Removing intermediate container e9b6636ca5fc
 ---> 919230b0a52c
Step 3/10 : ENV MYPATH /usr/local
 ---> Running in be1836a87769
Removing intermediate container be1836a87769
 ---> b41cabe51f12
Step 4/10 : WORKDIR $MYPATH
 ---> Running in 0a07ea2edf30
Removing intermediate container 0a07ea2edf30
 ---> 23f079bd17fe
Step 5/10 : RUN yum -y install vim
 ---> Running in ac8ddd990733
CentOS Linux 8 - AppStream                      2.5 MB/s | 8.2 MB     00:03    
CentOS Linux 8 - BaseOS                         6.9 MB/s | 3.5 MB     00:00    
CentOS Linux 8 - Extras                          19 kB/s |  10 kB     00:00    
Dependencies resolved.
================================================================================
 Package             Arch        Version                   Repository      Size
================================================================================
Installing:
 vim-enhanced        x86_64      2:8.0.1763-16.el8         appstream      1.4 M
Installing dependencies:
 gpm-libs            x86_64      1.20.7-17.el8             appstream       39 k
 vim-common          x86_64      2:8.0.1763-16.el8         appstream      6.3 M
 vim-filesystem      noarch      2:8.0.1763-16.el8         appstream       49 k
 which               x86_64      2.21-16.el8               baseos          49 k

Transaction Summary
================================================================================
Install  5 Packages

Total download size: 7.8 M
Installed size: 30 M
Downloading Packages:
(1/5): gpm-libs-1.20.7-17.el8.x86_64.rpm        182 kB/s |  39 kB     00:00    
(2/5): vim-filesystem-8.0.1763-16.el8.noarch.rp 850 kB/s |  49 kB     00:00    
(3/5): which-2.21-16.el8.x86_64.rpm             3.3 MB/s |  49 kB     00:00    
(4/5): vim-enhanced-8.0.1763-16.el8.x86_64.rpm  2.5 MB/s | 1.4 MB     00:00    
(5/5): vim-common-8.0.1763-16.el8.x86_64.rpm    4.8 MB/s | 6.3 MB     00:01    
--------------------------------------------------------------------------------
Total                                           1.4 MB/s | 7.8 MB     00:05     
warning: /var/cache/dnf/appstream-02e86d1c976ab532/packages/gpm-libs-1.20.7-17.el8.x86_64.rpm: Header V3 RSA/SHA256 Signature, key ID 8483c65d: NOKEY
CentOS Linux 8 - AppStream                      1.6 MB/s | 1.6 kB     00:00    
Importing GPG key 0x8483C65D:
 Userid     : "CentOS (CentOS Official Signing Key) <security@centos.org>"
 Fingerprint: 99DB 70FA E1D7 CE22 7FB6 4882 05B5 55B3 8483 C65D
 From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-centosofficial
Key imported successfully
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                        1/1 
  Installing       : which-2.21-16.el8.x86_64                               1/5 
  Installing       : vim-filesystem-2:8.0.1763-16.el8.noarch                2/5 
  Installing       : vim-common-2:8.0.1763-16.el8.x86_64                    3/5 
  Installing       : gpm-libs-1.20.7-17.el8.x86_64                          4/5 
  Running scriptlet: gpm-libs-1.20.7-17.el8.x86_64                          4/5 
  Installing       : vim-enhanced-2:8.0.1763-16.el8.x86_64                  5/5 
  Running scriptlet: vim-enhanced-2:8.0.1763-16.el8.x86_64                  5/5 
  Running scriptlet: vim-common-2:8.0.1763-16.el8.x86_64                    5/5 
  Verifying        : gpm-libs-1.20.7-17.el8.x86_64                          1/5 
  Verifying        : vim-common-2:8.0.1763-16.el8.x86_64                    2/5 
  Verifying        : vim-enhanced-2:8.0.1763-16.el8.x86_64                  3/5 
  Verifying        : vim-filesystem-2:8.0.1763-16.el8.noarch                4/5 
  Verifying        : which-2.21-16.el8.x86_64                               5/5 

Installed:
  gpm-libs-1.20.7-17.el8.x86_64         vim-common-2:8.0.1763-16.el8.x86_64    
  vim-enhanced-2:8.0.1763-16.el8.x86_64 vim-filesystem-2:8.0.1763-16.el8.noarch
  which-2.21-16.el8.x86_64             

Complete!
Removing intermediate container ac8ddd990733
 ---> a6577315b6be
Step 6/10 : RUN yum -y install net-tools
 ---> Running in 5457a870cbe1
Last metadata expiration check: 0:00:13 ago on Sat Dec 18 12:43:09 2021.
Dependencies resolved.
================================================================================
 Package         Architecture Version                        Repository    Size
================================================================================
Installing:
 net-tools       x86_64       2.0-0.52.20160912git.el8       baseos       322 k

Transaction Summary
================================================================================
Install  1 Package

Total download size: 322 k
Installed size: 942 k
Downloading Packages:
net-tools-2.0-0.52.20160912git.el8.x86_64.rpm    13 MB/s | 322 kB     00:00    
--------------------------------------------------------------------------------
Total                                           541 kB/s | 322 kB     00:00     
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                        1/1 
  Installing       : net-tools-2.0-0.52.20160912git.el8.x86_64              1/1 
  Running scriptlet: net-tools-2.0-0.52.20160912git.el8.x86_64              1/1 
  Verifying        : net-tools-2.0-0.52.20160912git.el8.x86_64              1/1 

Installed:
  net-tools-2.0-0.52.20160912git.el8.x86_64                                     

Complete!
Removing intermediate container 5457a870cbe1
 ---> ad7a1bc1b575
Step 7/10 : EXPOSE 80
 ---> Running in 09844d02a263
Removing intermediate container 09844d02a263
 ---> 1acbc9ed3dae
Step 8/10 : CMD echo $MYPATH
 ---> Running in 611857d76117
Removing intermediate container 611857d76117
 ---> 9f46616b9afc
Step 9/10 : CMD echo "--------end--------"
 ---> Running in 458bc4a138c1
Removing intermediate container 458bc4a138c1
 ---> 604c7c0ebab6
Step 10/10 : CMD /bin/bash
 ---> Running in 0b4881ba1861
Removing intermediate container 0b4881ba1861
 ---> b33b912056f8
Successfully built b33b912056f8
Successfully tagged mycentos:0.1
[root@iZuf67gjh27h6gyq7gvprtZ dockerfile]# docker images
REPOSITORY            TAG       IMAGE ID       CREATED              SIZE
mycentos              0.1       b33b912056f8   About a minute ago   322MB
liuhang/centos        1.0       7d6f62b954d5   2 hours ago          231MB
mysql                 5.7       738e7101490b   2 weeks ago          448MB
tomcat                1.0       cdf9a064371f   6 weeks ago          684MB
tomcat                9.0       43e421a14aec   8 weeks ago          680MB
tomcat                latest    b0e0b0a92cf9   8 weeks ago          680MB
redis                 latest    7faaec683238   2 months ago         113MB
nginx                 latest    87a94228f133   2 months ago         133MB
centos                latest    5d0da3dc9764   3 months ago         231MB
elasticsearch         7.14.2    2abd5342ace0   3 months ago         1.04GB
portainer/portainer   latest    580c0e4e98b0   9 months ago         79.1MB
elasticsearch         7.6.2     f29a1ee41030   21 months ago        791MB

# 3.测试运行 
# 直接容器后当前目录就是 WORKDIR地址， vim 和 ifconfig命令也有 
[root@iZuf67gjh27h6gyq7gvprtZ dockerfile]# docker run -it mycentos:0.1
[root@83f012d1d8f7 local]# pwd
/usr/local
[root@83f012d1d8f7 local]# ifconfig
eth0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 172.17.0.3  netmask 255.255.0.0  broadcast 172.17.255.255
        ether 02:42:ac:11:00:03  txqueuelen 0  (Ethernet)
        RX packets 8  bytes 656 (656.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        loop  txqueuelen 1000  (Local Loopback)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

[root@83f012d1d8f7 local]# vim test.txt
[root@83f012d1d8f7 local]# ls -l
total 44
drwxr-xr-x 2 root root 4096 Nov  3  2020 bin
drwxr-xr-x 2 root root 4096 Nov  3  2020 etc
drwxr-xr-x 2 root root 4096 Nov  3  2020 games
drwxr-xr-x 2 root root 4096 Nov  3  2020 include
drwxr-xr-x 2 root root 4096 Nov  3  2020 lib
drwxr-xr-x 3 root root 4096 Sep 15 14:17 lib64
drwxr-xr-x 2 root root 4096 Nov  3  2020 libexec
drwxr-xr-x 2 root root 4096 Nov  3  2020 sbin
drwxr-xr-x 5 root root 4096 Sep 15 14:17 share
drwxr-xr-x 2 root root 4096 Nov  3  2020 src
-rw-r--r-- 1 root root    4 Dec 18 12:51 test.txt
```



```shell
# docker history imageId
[root@iZuf67gjh27h6gyq7gvprtZ dockerfile]# docker images
REPOSITORY            TAG       IMAGE ID       CREATED          SIZE
mycentos              0.1       b33b912056f8   12 minutes ago   322MB
[root@iZuf67gjh27h6gyq7gvprtZ dockerfile]# docker history b33b912056f8
IMAGE          CREATED          CREATED BY                                      SIZE      COMMENT
b33b912056f8   13 minutes ago   /bin/sh -c #(nop)  CMD ["/bin/sh" "-c" "/bin…   0B        
604c7c0ebab6   13 minutes ago   /bin/sh -c #(nop)  CMD ["/bin/sh" "-c" "echo…   0B        
9f46616b9afc   13 minutes ago   /bin/sh -c #(nop)  CMD ["/bin/sh" "-c" "echo…   0B        
1acbc9ed3dae   13 minutes ago   /bin/sh -c #(nop)  EXPOSE 80                    0B        
ad7a1bc1b575   13 minutes ago   /bin/sh -c yum -y install net-tools             27MB      
a6577315b6be   13 minutes ago   /bin/sh -c yum -y install vim                   64MB      
23f079bd17fe   13 minutes ago   /bin/sh -c #(nop) WORKDIR /usr/local            0B        
b41cabe51f12   13 minutes ago   /bin/sh -c #(nop)  ENV MYPATH=/usr/local        0B        
919230b0a52c   13 minutes ago   /bin/sh -c #(nop)  MAINTAINER liuhang<435354…   0B        
5d0da3dc9764   3 months ago     /bin/sh -c #(nop)  CMD ["/bin/bash"]            0B        
<missing>      3 months ago     /bin/sh -c #(nop)  LABEL org.label-schema.sc…   0B        
<missing>      3 months ago     /bin/sh -c #(nop) ADD file:805cb5e15fb6e0bb0…   231MB     
```

### CMD和ENTRYPOIT区别

```shell
CMD              # 指定容器启动时候要执行的命令,只有最后一个会生效，可被替代
ENTRYPOIT        # 指定容器启动时候要执行的命令，可以追加命令
```

测试cmd

```shell
# 编写dockerfile文件
[root@iZuf67gjh27h6gyq7gvprtZ dockerfile]# vim dockerfile-cmd
[root@iZuf67gjh27h6gyq7gvprtZ dockerfile]# cat dockerfile-cmd 
FROM centos
CMD ["ls","-a"]
# 构建镜像
[root@iZuf67gjh27h6gyq7gvprtZ dockerfile]# docker build -f dockerfile-cmd -t cmdtest .

# 查看镜像
[root@iZuf67gjh27h6gyq7gvprtZ dockerfile]# docker images
REPOSITORY            TAG       IMAGE ID       CREATED          SIZE
cmdtest               latest    a1c5f28a134d   11 seconds ago   231MB
# 运行
[root@iZuf67gjh27h6gyq7gvprtZ dockerfile]# docker run a1c5f28a134d
.
..
.dockerenv
bin
dev
# 想追加一个命令 -l ls-al   cmd命令下 -l 替换了CMD ["ls","-a"]命令，-l 不是命令所有报错  
[root@iZuf67gjh27h6gyq7gvprtZ dockerfile]# docker run a1c5f28a134d -l
docker: Error response from daemon: OCI runtime create failed: container_linux.go:380: starting container process caused: exec: "-l": executable file not found in $PATH: unknown.
ERRO[0000] error waiting for container: context canceled 
```

测试ENTRYPOIT

```shell
# 1.编写dockerfile
[root@iZuf67gjh27h6gyq7gvprtZ dockerfile]# vim dockerfile-cmd-entrypoint
[root@iZuf67gjh27h6gyq7gvprtZ dockerfile]# cat dockerfile-cmd-entrypoint 
FROM centos
ENTRYPOINT ["ls","-a"]
# 2.构建镜像
[root@iZuf67gjh27h6gyq7gvprtZ dockerfile]# docker build -f dockerfile-cmd-entrypoint -t 
# 3.查看镜像
[root@iZuf67gjh27h6gyq7gvprtZ dockerfile]# docker images 
REPOSITORY            TAG       IMAGE ID       CREATED          SIZE
emtrytest             latest    ef76b4530add   19 seconds ago   231MB
cmdtest               latest    a1c5f28a134d   13 minutes ago   231MB
# 4.运行镜像
[root@iZuf67gjh27h6gyq7gvprtZ dockerfile]# docker run ef76b4530add
.
..
.dockerenv
bin
# 4.运行镜像 替换参数
[root@iZuf67gjh27h6gyq7gvprtZ dockerfile]# docker run ef76b4530add -al
total 56
drwxr-xr-x   1 root root 4096 Dec 18 13:23 .
drwxr-xr-x   1 root root 4096 Dec 18 13:23 ..
-rwxr-xr-x   1 root root    0 Dec 18 13:23 .dockerenv
lrwxrwxrwx   1 root root    7 Nov  3  2020 bin -> usr/bin
```

## 7.5 实战部署tomcat

```shell
# 1.编写Dockerfile
[root@iZuf67gjh27h6gyq7gvprtZ tomcat]# cat Dockerfile 
FROM centos
MAINTAINER liuhang<435354105@qq.com>
COPY readme.txt /usr/local/readme.txt
ADD jdk-8u111-linux-x64.tar.gz /usr/local/
ADD apache-tomcat-8.5.9.tar.gz /usr/local/
RUN yum -y install vim
ENV MYPATH /usr/local
WORKDIR $MYPATH
ENV JAVA_HOME /usr/local/jdk1.8.0_111
ENV CLASSPATH $JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar
ENV CATALINA_HOME /usr/local/apache-tomcat-8.5.9
ENV CATALINA_BASH /usr/local/apache-tomcat-8.5.9
ENV PATH $PATH:$JAVA_HOME/bin:$CATALINA_BASH/lib:$CATALINA_BASH/bin
EXPOSE 8080
CMD /usr/local/apache-tomcat-8.5.9/bin/startup.sh && tail -f /usr/local/apache-tomcat-8.5.9/logs/catalina.out 

# 2.编译
[root@iZuf67gjh27h6gyq7gvprtZ tomcat]# docker build -t diytomcat .
# 3.启动
[root@iZuf67gjh27h6gyq7gvprtZ tomcat]# docker run -d -p9090:8080 --name liuhangtomcat -v /home/dockerfile/tomcat/test/:/usr/local/apache-tomcat-8.5.9/webapps/test -v /home/dockerfile/tomcat/tomcatlogs/:/usr/local/apache-tomcat-8.5.9/logs diytomcat
850c0698de46323d65137f27792fb584f70cdb523d2751b561262a4823935508
[root@iZuf67gjh27h6gyq7gvprtZ tomcat]# docker ps
CONTAINER ID   IMAGE                 COMMAND                  CREATED         STATUS         PORTS                                       NAMES
850c0698de46   diytomcat             "/bin/sh -c '/usr/lo…"   6 seconds ago   Up 5 seconds   0.0.0.0:9090->8080/tcp, :::9090->8080/tcp   liuhangtomcat
1b7717fc1222   portainer/portainer   "/portainer"             6 weeks ago     Up 6 weeks     0.0.0.0:8088->9000/tcp, :::8088->9000/tcp   laughing_mahavira
```

TODO 31

# NOTE 

``` shell
# 1.删除镜像
docker rmi 镜像id 
# 2.删除容器
docker rm 容器id
# 3.查看容器日志
docker logs 容器id
# 4.进入容器
docker exec -it 850c0698de463 /bin/bash
```

